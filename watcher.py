#!/usr/bin/python
import os,sys
import requests
import jinja2
import schedule
import logging
import argparse
import base64
import signal
import subprocess
import json
import socket
from Crypto.PublicKey import RSA, ECC
from Crypto.Hash import *
from Crypto.Signature import PKCS1_v1_5
from urllib.parse import urlparse
import threading
import time


LOGGER = logging.getLogger('axiom_watcher')

REGISTERED = False

URI = "http://localhost"

KEY_PATH = "/opt/watcher/keys"

KEY_NAME = "test-key-ecdsa"

KEY_TYPE = "ec"

FREQUENCY = 1

VASSAL_PORT = 8080

VASSAL_HOST = "localhost"

CONEXT_ROOT = "/vassal"

HOSTNAME = VASSAL_HOST

REG_RETRIES = 10

SERVICES_CONFIG = {}

SERVICES_CONFIG_API_ETAG = ""


loadbalancer_reg_body = {
    "address": VASSAL_HOST,
    "name": socket.gethostname(),
    "services": []
}

service_register_body = {
    "address": "",
    "directives": [],
    "hostname": "",
    "port": 0,
    "type": ""
}

def job():
    LOGGER.info("job fired")
    global SERVICES_CONFIG
    rendered_file = os.getenv('LB_CONFIG_FILE',"./test_files/nginx.conf")
    template_env = load_templates_env()
    render_template(template_env,"nginx.j2",SERVICES_CONFIG,rendered_file)

def init_logger(level):
    formatter = None
    if level == "debug":
        LOGGER.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(threadName)-9s) %(message)s',)
    else:
        LOGGER.setLevel(logging.info)


    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    LOGGER.addHandler(ch)

def buildBaseURI():
    global VASSAL_HOST
    global VASSAL_PORT
    global URI
    LOGGER.info("building base URI")
    host = VASSAL_HOST
    port = VASSAL_PORT
    URI = "http://" + host + ":" + str(port) + CONEXT_ROOT
    LOGGER.debug("base URI is: " + URI)

def doConfigRequest(**kwargs):
    global REGISTERED
    global SERVICES_CONFIG
    global SERVICES_CONFIG_API_ETAG

    LOGGER.info("do config request")
    
    url = URI + "/api/services/" + str(kwargs['path']) # should validate URL here 
    
    headers = {
        'user-agent' : 'axiom_watcher',
        'Content-Type' : 'application/json',
        'ETag': SERVICES_CONFIG_API_ETAG 
    }

    params = {
        'name' : loadbalancer_reg_body['name']
    }
    
    LOGGER.debug("request headers: " + str(headers))

    try:

        response = requests.get(url, headers=headers,params=params)

        if 'ETag' in response.headers and response.headers['ETag'] != SERVICES_CONFIG_API_ETAG:
            SERVICES_CONFIG_API_ETAG = response.headers['ETag']
            LOGGER.debug("ETag updated: " + SERVICES_CONFIG_API_ETAG)

        if response.status_code == 200:

            LOGGER.debug("response code: " + str(response.status_code))
            SERVICES_CONFIG = response.json()
            LOGGER.debug(SERVICES_CONFIG)

        elif response.status_code == 304:

            LOGGER.debug("response code: " + str(response.status_code))
            LOGGER.debug("no change to dataset")

        else:

            LOGGER("response code: " + str(response.status_code))
    
    except requests.exceptions.RequestException as e:
        REGISTERED = False
        LOGGER.error(e)



"""
Perform a registration request
- requires service path
- requires body object 
"""
def doRegisterRequest(service_path,body):
    global REGISTERED

    url = URI + "/api/services/" + service_path # should validate URL here 
    headers = {'user-agent' : 'axiom_watcher','Content-Type' : 'application/json'}

    LOGGER.debug("request body: " + json.dumps(body))
    LOGGER.debug("request headers: " + str(headers))

    try:
        r = requests.post(url, data=json.dumps(body), headers=headers)


        LOGGER.debug("request response code " + str(r.status_code))

        if r.status_code == requests.codes.ok:
            REGISTERED = True

    except requests.exceptions.RequestException as e:
        LOGGER.error(e)




def load_templates_env():
    template_loader = jinja2.FileSystemLoader(searchpath="./templates/")
    template_env = jinja2.Environment(loader=template_loader)
    return template_env
       


def render_template(template_env,template_file,template_values,file):
    template = template_env.get_template(template_file)
    output = template.render(template_values)

    try:
        with open(file, 'w') as f:
            f.write(output)
    finally:
        f.close   


def read_file(file_name, flags):
    reader = open(file_name,flags)
    try:
        content = reader.read()
        return content
    finally:
        reader.close


def load_ssh_keys():
    LOGGER.info("load ssh keys")
    LOGGER.info("load ssh key path is " + KEY_PATH)

    path = KEY_PATH + KEY_NAME

    if KEY_TYPE == "rsa":
        try:
            private_key = RSA.import_key(read_file(path,"rt"))
            
        except:
            logging.error("error loading RSA key")

    elif KEY_TYPE == "ec":
        try:
            private_key = ECC.import_key(read_file(path,"rt"))
            
        except:
            logging.error("error loading EC key")
    else:
        logging.error("unknown key type")
        sys.exit(1)




def parse_args():
    global KEY_PATH
    global KEY_NAME
    global KEY_TYPE
    global VASSAL_HOST

    LOGGER.info("passing args")
    parser = argparse.ArgumentParser()
    parser.add_argument("--host",type=str, help="vassal host",required=False)
    parser.add_argument("--key_type",type=str, choices=["rsa","ec"],required=True)
    parser.add_argument("--key_path", type=str,help="path to private key file", required=False)
    parser.add_argument("--key_name", type=str,help="name of private key file", required=False)
    parser.add_argument("--uri", type=str,help="base uri", required=False)
    parser.add_argument("--frequency", type=int, help="config change check frequency")
    parser.add_argument("-v", type=str, help="verbosity",required=False)
    args = parser.parse_args()

    if args.v == "debug":
        init_logger("debug")

    if args.key_type != None:
        KEY_TYPE = args.key_type 

    if args.key_name != None:
        KEY_NAME = args.key_name    

    if args.key_path != None:
        KEY_PATH = args.key_path

    if args.host != None:
        VASSAL_HOST = args.host

def parse_env():
    LOGGER.info("parsing environment variables")
    global KEY_PATH
    global KEY_NAME
    global KEY_TYPE
    global VASSAL_HOST
    global VASSAL_PORT

    if os.getenv('DEBUG') != None:
        init_logger("debug")

    if os.getenv('KEY_TYPE') != None:
        KEY_TYPE = os.getenv('KEY_TYPE')

    if os.getenv('KEY_NAME') != None:
        KEY_NAME = os.getenv('KEY_NAME')   

    if os.getenv('KEY_PATH') != None:
        KEY_PATH = os.getenv('KEY_PATH') 

    if os.getenv('VASSAL_HOST') != None:
        VASSAL_HOST = os.getenv('VASSAL_HOST')

    if os.getenv('VASSAL_PORT') != None:
        VASSAL_PORT = os.getenv('VASSAL_PORT') 


def run_continuously(job_func,a):
    
    job_thread = threading.Thread(target=job_func, kwargs=a)
    job_thread.setDaemon(True)
    job_thread.start()


def bootstrap():

    global REG_RETRIES

    load_ssh_keys()

    buildBaseURI()    

    #do registration
    doRegisterRequest("loadbalancer",loadbalancer_reg_body)


    retry = 0 
    while not REGISTERED and retry < REG_RETRIES:
        LOGGER.warning("retrying registration count " + str(retry))
        doRegisterRequest("loadbalancer",loadbalancer_reg_body)
        retry = retry + 1
        time.sleep(5)

    #if registration is success schedule key watch
    if not REGISTERED and retry == REG_RETRIES:
        LOGGER.error("unable to register after " + str(retry) + " attempts")
        sys.exit(1)

    # schedule jobs
    schedule.every(1).seconds.do(run_continuously, doConfigRequest,dict(path="loadbalancer"))
    #schedule.every(1).seconds.do(run_continuously,job,{})
    run_continuously(detect_changes,{})


def detect_changes():
    service_tag = SERVICES_CONFIG_API_ETAG
    while True:
        if service_tag != SERVICES_CONFIG_API_ETAG:
            LOGGER.debug("service has changed")  
            job()  
            service_tag = SERVICES_CONFIG_API_ETAG
        time.sleep(1)



def main():
    parse_args()
    parse_env()
    LOGGER.info("axiom agent starting up")

    # boostrap the watcher / register with axiom 
    bootstrap()




    # main loop
    while 1:
        schedule.run_pending()
        time.sleep(1)
        if not REGISTERED:
            LOGGER.warning("rebootstraping...")
            schedule.clear() 
            bootstrap()
            time.sleep(10)

        # send reload signals

    #Store result 

    #on change do update config

        # build config 

        # send reload signals

    file = "./test_files/nginx.conf"
    value = {"something" : "test text"}
    template_env = load_templates_env()
    render_template(template_env,"nginx.j2",value,file)




main()

